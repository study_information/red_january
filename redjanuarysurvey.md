# RED January Survey

## INFORMATION AND CONSENT

You are being invited to take part in a research study about your physical activity and mental health, and about your participation in RED January 2022. The study will involve answering a 10-minute survey now and two follow-up surveys of similar length in February and July.

The project will help us understand how taking part in RED January might improve people’s wellbeing, and how being active every day might support people’s mental health in the community. It will also help us learn more about sport, exercise and physical activity at the University. The data you provide will be analysed by researchers at the University of Oxford.

By taking part, you are agreeing that you have read and understood the information about the study below. Please ensure you have read and understood this information before continuing.

### Do I have to take part?

Participation is open to people over the age of 18 who have registered for RED January 2022 and who are staff, rseaechers or students at the University of Oxford – and is entirely voluntary. **Participation or non-participation will not affect your academic assessment or employment in any way.** You can ask any questions before deciding to take part by contacting the research team (details below). You can opt out of taking part in future surveys by ignoring the follow-up invitation - you will then be automatically withdrawn. You can also withdraw from a survey at any point and for any reason before submitting your answers by simply closing the browser.

### What will taking part involve?

You will be asked to complete one online survey now and then we will send you two further surveys at the start of February and in July. You will be asked questions about yourself, your physical activity and your psychological wellbeing. Each survey will take approximately ten minutes to complete and no background knowledge is required.

### What will happen to the information I give you?

The data we will collect that could identify you will be your gender, age and position at the university. Some questions are considered sensitive data, such as those about psychological wellbeing. We will also ask you for your email address so that we can contact you for follow-up surveys. This email address will be removed from the rest of the answers you give before any analysis takes place and we will delete it as soon as the study finishes. Your email address will not be passed to any third parties and your IP address will not be stored. Your anonymous data will be stored in a password-protected electronic file, and may be used in scientific papers. Nobody will be able to identify you from the
anonymous data we analyse, or from any publications. Research data (including consent records) will be stored for three years after publication or public release. It will not be possible to remove your information from existing data sets once those data sets have been anonymised, because we would be unable to identify your data.

### Who will have access to my data?

The University of Oxford is the data controller with respect to your personal data, and as such will determine how your personal data is used in the study. The University will process your personal data for the purpose of the research outlined above. **No identifiable information arising from the research will be disclosed to your college, department or faculty.** Research is a task that we perform in the public interest. Further information about your rights with respect to your personal data is available from <https://compliance.admin.ox.ac.uk/individual-rights>.

### Who has reviewed this study?

This project has been reviewed by, and received ethics clearance through, the University of Oxford Central University Research Ethics Committee R67006/RE001.

### Questions and Concerns

If you have any questions or concerns about the study, you can contact the study team at <redjanuary@ndcn.ox.ac.uk> or email the principal researcher, Dr Catherine Wheatley, at <catherine.wheatley@ndcn.ox.ac.uk>. She will acknowledge your concern within 10 working days and give you an indication of how it will be dealt with. If you remain unhappy or wish to make a formal complaint, please contact the Medical Sciences Interdivisional Research Ethics Committee at the University of Oxford at <ethics@medsci.ox.ac.uk> or write to Research Services, University of
Oxford, Wellington Square, Oxford OX1 2JD.

### Consent

#### I understand that:

My participation is completely voluntary. I will need to provide an email address so that I can be sent the future surveys to answer. However, this email address will not be passed to any third parties and will be removed from my answers before any analysis takes place, so the information I provide will be anonymised. Due to this anonymisation it will not be possible to withdraw my answers after they have been submitted, but I can withdraw from future surveys at any point.The data gathered in this study will be stored securely and it will not be possible to identify me in any outputs from this research.

